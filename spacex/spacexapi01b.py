#!/usr/bin/python3

import requests

from colorama import init, Fore, Back, Style

init()

SPACEXURI = "https://api.spacexdata.com/v3/cores"


def main():
    coreData = requests.get(SPACEXURI).json()

    displayString = ""

    for core in coreData:
        displayString += f"{Fore.GREEN}Core Serial: {Fore.WHITE}{core.get('core_serial')}\n{Fore.GREEN}Original Launch: {Fore.WHITE}{core.get('original_launch')}\n{Fore.GREEN}Details: {Fore.WHITE}{core.get('details')}\n{Fore.GREEN}Status: {Fore.WHITE}{core.get('status')}"

        listOfMissions = core.get("missions")
        if (len(listOfMissions) > 0):
            displayString += f"\n{Fore.GREEN}Missions:"

            for mission in listOfMissions:
                displayString += f"\n\t{Fore.YELLOW}Name: {Fore.WHITE}{mission.get('name')}, {Fore.YELLOW}Flight: {Fore.WHITE}{mission.get('flight')}"

        displayString += "\n\n"

    print(displayString)
    # for core in coreData:
    #     displayString += f"{Fore.GREEN}Core Serial: "
    #
    # for core in listofCores:
    #     print(core, end="\n\n")
    #     print(f"The core serial fo this launch is {core['core_serial']}")
    #     print(f"The details are {core['details']}")


if __name__ == "__main__":
    main()

