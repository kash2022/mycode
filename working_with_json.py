#!/usr/bin/python3

"""
JSON Libraby should be availalbe
"""

import json

# json.load() - load a json file and translate to Pythonic data (unmarshar from file)
# json.loads - loads a json str and translate to pythonic data (umarshal from string)
# json.dump - transform (marshal) from pythonic data to legal JSON in a file
# json.dumps - transfom*(marshal) from pythonic data to legal json str

def main():
    """  run=time code """

    # open up your json file
    with open("issdata.json", "r") as issdata:
        # Read out our data if we can, translate at same time to pythonic data
        issdata_py = json.load(issdata)
        
    # proof of concept, print on screen
    print(type(issdata_py))
    print(issdata_py.get("timestamp"))


if __name__ == "__main__" :
    main()

